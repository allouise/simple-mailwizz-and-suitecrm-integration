=== Simple MailWizz and SuiteCRM Integration ===
Donate link: https://paypal.me/allouise
Tags: mailwizz, suitecrm, integration, api, table, translatable, poedit
Requires at least: 5.0
Tested up to: 5.3.2
Stable tag: 5.3.2
Requires PHP: 5.4
License: GPLv2 or later
License URI: https://www.gnu.org/licenses/gpl-2.0.html

Simple MailWizz & SuiteCRM integration plugin. MailWizz: Displays Number of Campaigns, Templates, Listings & Subscribers. SuiteCRM: Displays Number of Leads, Top 10 Opportunities, Calls & Meetings.

Plugin is Translatable ready via PoEdit

== Description ==

Simple MailWizz & SuiteCRM integration plugin. MailWizz: Displays Number of Campaigns, Templates, Listings & Subscribers. SuiteCRM: Displays Number of Leads, Top 10 Opportunities, Calls & Meetings.

Plugin is Translatable ready via PoEdit

Please post bugs here: https://bitbucket.org/allouise/simple-mailwizz-and-suitecrm-integration

== Installation ==

1. Upload the plugin files to the `/wp-content/plugins/plugin-name` directory, or install the plugin through the WordPress plugins screen directly.
2. Activate the plugin through the 'Plugins' screen in WordPress
3. MailWizz: Obtain your MailWizz Public & Private Keys
4. SuiteCRM: Will require Username, Password & SuiteCRM login URL
5. Use the MailWizz & SuiteCRM Settings page to configure the plugin & enter your API credentials

== Screenshots ==

1. Simple MailWizz and SuiteCRM Integration

== Changelog ==

= 0.1.0 (Feb 27, 2020) =

- First release.