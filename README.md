## Simple MailWizz and SuiteCRM Integration
Requires at least: 5.0
Tested up to: 5.3.2
Stable tag: 5.3.2

Custom Plugin Created for a Client that requires MailWizz & SuiteCRM Integration on their backend.
---

### Description

Simple MailWizz & SuiteCRM integration plugin. MailWizz: Displays Number of Campaigns, Templates, Listings & Subscribers. SuiteCRM: Displays Number of Leads, Top 10 Opportunities, Calls & Meetings.

Plugin is Translatable ready via PoEdit

1. Upload the plugin files to the `/wp-content/plugins/plugin-name` directory, or install the plugin through the WordPress plugins screen directly.
2. Activate the plugin through the 'Plugins' screen in WordPress
3. MailWizz: Obtain your MailWizz Public & Private Keys
4. SuiteCRM: Will require Username, Password & SuiteCRM login URL
5. Use the MailWizz & SuiteCRM Settings page to configure the plugin & enter your API credentials

#### For Wordpress Customization Contact me at elixirlouise@gmail.com

---

### Screenshots

#### Settings Page
![picture](bit_assets/Settings.png)

#### Dashboard Page
![picture](bit_assets/Dashboard.png)

#### Dashboard Widget Page
![picture](bit_assets/DashboardWidget.png)

---