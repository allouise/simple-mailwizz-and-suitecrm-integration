<?php

/**
 * @link              https://bitbucket.org/allouise/simple-mailwizz-and-suitecrm-integration
 * @since             1.0.0
 * @package           ASimple_MailSuite
 *
 * @wordpress-plugin
 * Plugin Name:       Simple MailWizz and SuiteCRM Integration
 * Plugin URI:        https://bitbucket.org/allouise/simple-mailwizz-and-suitecrm-integration/
 * Description:       Simple MailWizz & SuiteCRM integration plugin. MailWizz: Displays Number of Campaigns, Templates, Listings & Subscribers. SuiteCRM: Displays Number of Leads, Top 10 Opportunities, Calls & Meetings.
 * Version:           1.0.0
 * Author:            Allyson Flores
 * Author URI:        http://allysonflores.com/
 * License:           GPL-2.0+
 * License URI:       http://www.gnu.org/licenses/gpl-2.0.txt
 * Text Domain:       asimple-mailsuite
 * Domain Path:       /languages
 */

// If this file is called directly, abort.
if ( ! defined( 'WPINC' ) ) {
	die;
}

/**
 * Currently plugin version.
 * Start at version 1.0.0 and use SemVer - https://semver.org
 * Rename this for your plugin and update it as you release new versions.
 */
define( 'ASimple_MailSuite_VERSION', '1.0.0' );
define( 'ASimple_MailSuite_Title', 'MailWizz & SuiteCRM' );

/**
 * The code that runs during plugin activation.
 * This action is documented in includes/class-asimple-mailsuite-activator.php
 */
function activate_ASimple_MailSuite() {
	require_once plugin_dir_path( __FILE__ ) . 'includes/class-asimple-mailsuite-activator.php';
	ASimple_MailSuite_Activator::activate();
}

/**
 * The code that runs during plugin deactivation.
 * This action is documented in includes/class-asimple-mailsuite-deactivator.php
 */
function deactivate_ASimple_MailSuite() {
	require_once plugin_dir_path( __FILE__ ) . 'includes/class-asimple-mailsuite-deactivator.php';
	ASimple_MailSuite_Deactivator::deactivate();
}

register_activation_hook( __FILE__, 'activate_ASimple_MailSuite' );
register_deactivation_hook( __FILE__, 'deactivate_ASimple_MailSuite' );

/**
 * The core plugin class that is used to define internationalization,
 * admin-specific hooks, and public-facing site hooks.
 */
require plugin_dir_path( __FILE__ ) . 'includes/class-asimple-mailsuite.php';

/**
 * Begins execution of the plugin.
 *
 * Since everything within the plugin is registered via hooks,
 * then kicking off the plugin from this point in the file does
 * not affect the page life cycle.
 *
 * @since    1.0.0
 */
function run_ASimple_MailSuite() {

	$plugin = new ASimple_MailSuite();
	$plugin->run();

}
run_ASimple_MailSuite();
