<?php

/**
 * Fired during plugin deactivation
 *
 * @link       https://bitbucket.org/allouise/simple-mailwizz-and-suitecrm-integration
 * @since      1.0.0
 *
 * @package    ASimple_MailSuite
 * @subpackage ASimple_MailSuite/includes
 */

/**
 * Fired during plugin deactivation.
 *
 * This class defines all code necessary to run during the plugin's deactivation.
 *
 * @since      1.0.0
 * @package    ASimple_MailSuite
 * @subpackage ASimple_MailSuite/includes
 * @author     Allyson Flores <elixirlouise@gmail.com>
 */
class ASimple_MailSuite_Deactivator {

	/**
	 * Short Description. (use period)
	 *
	 * Long Description.
	 *
	 * @since    1.0.0
	 */
	public static function deactivate() {

	}

}
