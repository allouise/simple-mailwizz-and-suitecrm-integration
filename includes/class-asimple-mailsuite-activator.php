<?php

/**
 * Fired during plugin activation
 *
 * @link       https://bitbucket.org/allouise/simple-mailwizz-and-suitecrm-integration
 * @since      1.0.0
 *
 * @package    ASimple_MailSuite
 * @subpackage ASimple_MailSuite/includes
 */

/**
 * Fired during plugin activation.
 *
 * This class defines all code necessary to run during the plugin's activation.
 *
 * @since      1.0.0
 * @package    ASimple_MailSuite
 * @subpackage ASimple_MailSuite/includes
 * @author     Allyson Flores <elixirlouise@gmail.com>
 */
class ASimple_MailSuite_Activator {

	/**
	 * Short Description. (use period)
	 *
	 * Long Description.
	 *
	 * @since    1.0.0
	 */
	public static function activate() {

	}

}
