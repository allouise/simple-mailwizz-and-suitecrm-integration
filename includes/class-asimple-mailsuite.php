<?php

/**
 * The file that defines the core plugin class
 *
 * A class definition that includes attributes and functions used across both the
 * public-facing side of the site and the admin area.
 *
 * @link       https://bitbucket.org/allouise/simple-mailwizz-and-suitecrm-integration
 * @since      1.0.0
 *
 * @package    ASimple_MailSuite
 * @subpackage ASimple_MailSuite/includes
 */

/**
 * The core plugin class.
 *
 * This is used to define internationalization, admin-specific hooks, and
 * public-facing site hooks.
 *
 * Also maintains the unique identifier of this plugin as well as the current
 * version of the plugin.
 *
 * @since      1.0.0
 * @package    ASimple_MailSuite
 * @subpackage ASimple_MailSuite/includes
 * @author     Allyson Flores <elixirlouise@gmail.com>
 */
class ASimple_MailSuite {

	/**
	 * The loader that's responsible for maintaining and registering all hooks that power
	 * the plugin.
	 *
	 * @since    1.0.0
	 * @access   protected
	 * @var      ASimple_MailSuite_Loader    $loader    Maintains and registers all hooks for the plugin.
	 */
	protected $loader;

	/**
	 * The unique identifier of this plugin.
	 *
	 * @since    1.0.0
	 * @access   protected
	 * @var      string    $ASimple_MailSuite    The string used to uniquely identify this plugin.
	 */
	protected $ASimple_MailSuite;

	/**
	 * The current version of the plugin.
	 *
	 * @since    1.0.0
	 * @access   protected
	 * @var      string    $version    The current version of the plugin.
	 */
	protected $version;

	/**
	 * The plugin name handler.
	 *
	 * @since    1.0.0
	 * @access   protected
	 * @var      string    $plugin_name    The plugin name unique slug.
	 */
	protected $plugin_name;

	/**
	 * Define the core functionality of the plugin.
	 *
	 * Set the plugin name and the plugin version that can be used throughout the plugin.
	 * Load the dependencies, define the locale, and set the hooks for the admin area and
	 * the public-facing side of the site.
	 *
	 * @since    1.0.0
	 */
	public function __construct() {
		if ( defined( 'ASimple_MailSuite_VERSION' ) ) {
			$this->version = ASimple_MailSuite_VERSION;
		} else {
			$this->version = '1.0.0';
		}
		$this->ASimple_MailSuite = 'asimple-mailsuite';
		$this->plugin_name = 'asimple_mailsuite';

		$this->load_dependencies();
		$this->set_locale();
		$this->define_admin_hooks();
		/*$this->define_public_hooks();*/

	}

	/**
	 * Load the required dependencies for this plugin.
	 *
	 * Include the following files that make up the plugin:
	 *
	 * - ASimple_MailSuite_Loader. Orchestrates the hooks of the plugin.
	 * - ASimple_MailSuite_i18n. Defines internationalization functionality.
	 * - ASimple_MailSuite_Admin. Defines all hooks for the admin area.
	 * - ASimple_MailSuite_Public. Defines all hooks for the public side of the site.
	 *
	 * Create an instance of the loader which will be used to register the hooks
	 * with WordPress.
	 *
	 * @since    1.0.0
	 * @access   private
	 */
	private function load_dependencies() {

		/**
		 * The class responsible for orchestrating the actions and filters of the
		 * core plugin.
		 */
		require_once plugin_dir_path( dirname( __FILE__ ) ) . 'includes/class-asimple-mailsuite-loader.php';

		/**
		 * The class responsible for defining internationalization functionality
		 * of the plugin.
		 */
		require_once plugin_dir_path( dirname( __FILE__ ) ) . 'includes/class-asimple-mailsuite-i18n.php';

		/**
		 * The class responsible for defining all actions that occur in the admin area.
		 */
		require_once plugin_dir_path( dirname( __FILE__ ) ) . 'admin/class-asimple-mailsuite-admin.php';

		/**
		 * MailWiz Api definition.
		 */
		require_once plugin_dir_path( dirname( __FILE__ ) ) . 'libraries/ASM_MailWizzApi/Autoloader.php';

		/**
		 * The class responsible for defining all actions that occur in the public-facing
		 * side of the site.
		 */
		/*require_once plugin_dir_path( dirname( __FILE__ ) ) . 'public/class-asimple-mailsuite-public.php';*/

		$this->loader = new ASimple_MailSuite_Loader();

	}

	/**
	 * Define the locale for this plugin for internationalization.
	 *
	 * Uses the ASimple_MailSuite_i18n class in order to set the domain and to register the hook
	 * with WordPress.
	 *
	 * @since    1.0.0
	 * @access   private
	 */
	private function set_locale() {

		$plugin_i18n = new ASimple_MailSuite_i18n();

		$this->loader->add_action( 'plugins_loaded', $plugin_i18n, 'load_plugin_textdomain' );

	}

	/**
	 * Register all of the hooks related to the admin area functionality
	 * of the plugin.
	 *
	 * @since    1.0.0
	 * @access   private
	 */
	private function define_admin_hooks() {

		$plugin_admin = new ASimple_MailSuite_Admin( $this->get_ASimple_MailSuite(), $this->get_version(), $this->plugin_name );

		// if( isset($_GET['page']) && in_array($_GET['page'], array( $this->plugin_name, $this->plugin_name."_settings" )) ){
			$this->loader->add_action( 'admin_enqueue_scripts', $plugin_admin, 'enqueue_styles' );
			$this->loader->add_action( 'admin_enqueue_scripts', $plugin_admin, 'enqueue_scripts' );
		// }
		$this->loader->add_action( 'wp_ajax_asimple_mailsuite_admin_ajax', $plugin_admin, 'load_tables' );
		$this->loader->add_action( 'admin_menu', $plugin_admin, 'add_admin_menu_page' );
		$this->loader->add_action( 'admin_init', $plugin_admin, 'register_setting' );
		$this->loader->add_action( 'wp_dashboard_setup', $plugin_admin, 'dashboard_widgets' );
		$this->loader->add_filter( 'plugin_action_links_'.$this->get_ASimple_MailSuite().'/'.$this->get_ASimple_MailSuite().'.php', $plugin_admin, 'add_settings_link_plugin', 10, 4 );
	}

	/**
	 * Register all of the hooks related to the public-facing functionality
	 * of the plugin.
	 *
	 * @since    1.0.0
	 * @access   private
	 */
	/*private function define_public_hooks() {

		$plugin_public = new ASimple_MailSuite_Public( $this->get_ASimple_MailSuite(), $this->get_version() );

		$this->loader->add_action( 'wp_enqueue_scripts', $plugin_public, 'enqueue_styles' );
		$this->loader->add_action( 'wp_enqueue_scripts', $plugin_public, 'enqueue_scripts' );

	}*/

	/**
	 * Run the loader to execute all of the hooks with WordPress.
	 *
	 * @since    1.0.0
	 */
	public function run() {
		$this->loader->run();
	}

	/**
	 * The name of the plugin used to uniquely identify it within the context of
	 * WordPress and to define internationalization functionality.
	 *
	 * @since     1.0.0
	 * @return    string    The name of the plugin.
	 */
	public function get_ASimple_MailSuite() {
		return $this->ASimple_MailSuite;
	}

	/**
	 * The reference to the class that orchestrates the hooks with the plugin.
	 *
	 * @since     1.0.0
	 * @return    ASimple_MailSuite_Loader    Orchestrates the hooks of the plugin.
	 */
	public function get_loader() {
		return $this->loader;
	}

	/**
	 * Retrieve the version number of the plugin.
	 *
	 * @since     1.0.0
	 * @return    string    The version number of the plugin.
	 */
	public function get_version() {
		return $this->version;
	}

}
