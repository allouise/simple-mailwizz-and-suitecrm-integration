msgid ""
msgstr ""
"Project-Id-Version: \n"
"POT-Creation-Date: 2020-02-27 20:03+0800\n"
"PO-Revision-Date: 2020-02-27 20:03+0800\n"
"Last-Translator: \n"
"Language-Team: \n"
"Language: en\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"X-Generator: Poedit 2.2.3\n"
"X-Poedit-Basepath: ..\n"
"Plural-Forms: nplurals=2; plural=(n != 1);\n"
"X-Poedit-KeywordsList: _e;__;Additional keywords\n"
"X-Poedit-SearchPath-0: .\n"

#: admin/class-asimple-mailsuite-admin.php:137
#: admin/class-asimple-mailsuite-admin.php:259
#: admin/partials/asimple-mailsuite-dashboard-display.php:115
msgid "Settings"
msgstr "Settings"

#: admin/class-asimple-mailsuite-admin.php:147
#: admin/class-asimple-mailsuite-admin.php:258
#: admin/partials/asimple-mailsuite-dashboard-display.php:114
msgid "Dashboard"
msgstr "Dashboard"

#: admin/class-asimple-mailsuite-admin.php:271
#, fuzzy
#| msgid "Please setup your correct ComCart EMA API Details"
msgid "Please setup your correct MailWizz API Details"
msgstr "Please setup your correct ComCart EMA API Details"

#: admin/class-asimple-mailsuite-admin.php:273
#, fuzzy
#| msgid "Please setup your correct ComCart CRM Login Details"
msgid "Please setup your correct SuiteCRM Login Details"
msgstr "Please setup your correct ComCart CRM Login Details"

#: admin/class-asimple-mailsuite-admin.php:312
msgid ""
"Invalid Request please refresh this page. If problem persist contact "
"administrator."
msgstr ""
"Invalid Request please refresh this page. If problem persist contact "
"administrator."

#: admin/class-asimple-mailsuite-admin.php:346
#: admin/class-asimple-mailsuite-admin.php:493
msgid "Viewing Top 10 Data"
msgstr "Viewing Top 10 Data"

#: admin/class-asimple-mailsuite-admin.php:346
#: admin/class-asimple-mailsuite-admin.php:493
msgid "View More"
msgstr "View More"

#: admin/class-asimple-mailsuite-admin.php:370
msgid "Unique ID"
msgstr "Unique ID"

#: admin/class-asimple-mailsuite-admin.php:371
msgid "Visible Name"
msgstr "Visible Name"

#: admin/class-asimple-mailsuite-admin.php:372
#: admin/class-asimple-mailsuite-admin.php:437
#: admin/class-asimple-mailsuite-admin.php:459
#: admin/class-asimple-mailsuite-admin.php:507
#: admin/class-asimple-mailsuite-admin.php:520
msgid "Name"
msgstr "Name"

#: admin/class-asimple-mailsuite-admin.php:373
msgid "Description"
msgstr "Description"

#: admin/class-asimple-mailsuite-admin.php:383
msgid "First Name"
msgstr "First Name"

#: admin/class-asimple-mailsuite-admin.php:384
msgid "Last Name"
msgstr "Last Name"

#: admin/class-asimple-mailsuite-admin.php:385
msgid "Email"
msgstr "Email"

#: admin/class-asimple-mailsuite-admin.php:386
msgid "Age"
msgstr "Age"

#: admin/class-asimple-mailsuite-admin.php:387
msgid "Date Added"
msgstr "Date Added"

#: admin/class-asimple-mailsuite-admin.php:388
#: admin/class-asimple-mailsuite-admin.php:438
#: admin/class-asimple-mailsuite-admin.php:508
msgid "Status"
msgstr "Status"

#: admin/class-asimple-mailsuite-admin.php:410
#: admin/class-asimple-mailsuite-admin.php:421
#: admin/partials/asimple-mailsuite-dashboard-display.php:48
msgid "Subscribers"
msgstr "Subscribers"

#: admin/class-asimple-mailsuite-admin.php:410
#: admin/class-asimple-mailsuite-admin.php:418
#: admin/class-asimple-mailsuite-admin.php:421
#: admin/class-asimple-mailsuite-admin.php:443
#: admin/class-asimple-mailsuite-admin.php:465
#: admin/class-asimple-mailsuite-admin.php:572
#: admin/partials/asimple-mailsuite-admin-display.php:32
msgid "Error"
msgstr "Error"

#: admin/class-asimple-mailsuite-admin.php:418
#: admin/partials/asimple-mailsuite-dashboard-display.php:44
msgid "Listings"
msgstr "Listings"

#: admin/class-asimple-mailsuite-admin.php:436
msgid "Campaign ID"
msgstr "Campaign ID"

#: admin/class-asimple-mailsuite-admin.php:443
#: admin/partials/asimple-mailsuite-dashboard-display.php:36
msgid "Campaigns"
msgstr "Campaigns"

#: admin/class-asimple-mailsuite-admin.php:458
msgid "Template ID"
msgstr "Template ID"

#: admin/class-asimple-mailsuite-admin.php:460
msgid "Screenshot"
msgstr "Screenshot"

#: admin/class-asimple-mailsuite-admin.php:465
#: admin/partials/asimple-mailsuite-dashboard-display.php:40
msgid "Templates"
msgstr "Templates"

#: admin/class-asimple-mailsuite-admin.php:509
#: admin/class-asimple-mailsuite-admin.php:521
msgid "Account Name"
msgstr "Account Name"

#: admin/class-asimple-mailsuite-admin.php:510
msgid "Office Phone"
msgstr "Office Phone"

#: admin/class-asimple-mailsuite-admin.php:511
msgid "Primary Email"
msgstr ""

#: admin/class-asimple-mailsuite-admin.php:512
#, fuzzy
#| msgid "Email"
msgid "Email 1"
msgstr "Email"

#: admin/class-asimple-mailsuite-admin.php:513
#, fuzzy
#| msgid "Email"
msgid "Email 2"
msgstr "Email"

#: admin/class-asimple-mailsuite-admin.php:514
#: admin/class-asimple-mailsuite-admin.php:525
msgid "User"
msgstr "User"

#: admin/class-asimple-mailsuite-admin.php:515
#: admin/class-asimple-mailsuite-admin.php:526
#: admin/class-asimple-mailsuite-admin.php:536
#: admin/class-asimple-mailsuite-admin.php:546
msgid "Date Created"
msgstr "Date Created"

#: admin/class-asimple-mailsuite-admin.php:522
msgid "Sales Stage"
msgstr "Sales Stage"

#: admin/class-asimple-mailsuite-admin.php:523
msgid "Amount"
msgstr "Amount"

#: admin/class-asimple-mailsuite-admin.php:524
msgid "Close"
msgstr "Close"

#: admin/class-asimple-mailsuite-admin.php:531
msgid "Direction"
msgstr "Direction"

#: admin/class-asimple-mailsuite-admin.php:532
#: admin/class-asimple-mailsuite-admin.php:541
msgid "Subject"
msgstr "Subject"

#: admin/class-asimple-mailsuite-admin.php:533
#: admin/class-asimple-mailsuite-admin.php:543
msgid "Related To"
msgstr "Related To"

#: admin/class-asimple-mailsuite-admin.php:534
#: admin/class-asimple-mailsuite-admin.php:544
msgid "Start Date"
msgstr "Start Date"

#: admin/class-asimple-mailsuite-admin.php:535
msgid "Assigned to"
msgstr "Assigned to"

#: admin/class-asimple-mailsuite-admin.php:542
msgid "Contact"
msgstr "Contact"

#: admin/class-asimple-mailsuite-admin.php:545
msgid "Assigned User"
msgstr "Assigned User"

#: admin/class-asimple-mailsuite-admin.php:609
msgid "MailWizz Login URL"
msgstr "MailWizz Login URL"

#: admin/class-asimple-mailsuite-admin.php:619
msgid "Public Key"
msgstr "Public Key"

#: admin/class-asimple-mailsuite-admin.php:629
msgid "Private Key"
msgstr "Private Key"

#: admin/class-asimple-mailsuite-admin.php:645
#, fuzzy
#| msgid "ComCart CRM Login URL"
msgid "SuiteCRM Login URL"
msgstr "ComCart CRM Login URL"

#: admin/class-asimple-mailsuite-admin.php:655
#, fuzzy
#| msgid "ComCart CRM Username"
msgid "SuiteCRM Username"
msgstr "ComCart CRM Username"

#: admin/class-asimple-mailsuite-admin.php:665
#, fuzzy
#| msgid "ComCart CRM Password"
msgid "SuiteCRM Password"
msgstr "ComCart CRM Password"

#: admin/class-asimple-mailsuite-admin.php:683
msgid "MailWizz API Credentials"
msgstr "MailWizz API Credentials"

#: admin/class-asimple-mailsuite-admin.php:728
#, fuzzy
#| msgid "ComCart CRM API Credentials"
msgid "SuiteCRM API Credentials"
msgstr "ComCart CRM API Credentials"

#: admin/partials/asimple-mailsuite-admin-display.php:29
#: admin/partials/asimple-mailsuite-dashboard-display.php:27
#, fuzzy
#| msgid "MailWizz Login URL"
msgid "MailWizz"
msgstr "MailWizz Login URL"

#: admin/partials/asimple-mailsuite-admin-display.php:32
#: admin/partials/asimple-mailsuite-dashboard-display.php:60
msgid "SuiteCRM"
msgstr ""

#: admin/partials/asimple-mailsuite-dashboard-display.php:68
msgid "Leads"
msgstr "Leads"

#: admin/partials/asimple-mailsuite-dashboard-display.php:72
msgid "Top 10 Opportunities"
msgstr "Top 10 Opportunities"

#: admin/partials/asimple-mailsuite-dashboard-display.php:76
msgid "Calls"
msgstr "Calls"

#: admin/partials/asimple-mailsuite-dashboard-display.php:80
msgid "Meetings"
msgstr "Meetings"

#~ msgid " Settings"
#~ msgstr " Settings"

#~ msgid "ComCart EMA Login URL"
#~ msgstr "ComCart EMA Login URL"

#~ msgid "ComCart EMA API Credentials"
#~ msgstr "ComCart EMA API Credentials"

#~ msgid "ComCart EMA"
#~ msgstr "ComCart EMA"

#~ msgid "ComCart CRM"
#~ msgstr "ComCart CRM"

#~ msgid "API URL"
#~ msgstr "API URL"
