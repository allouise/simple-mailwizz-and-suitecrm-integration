<?php
/**
 * This file contains the lists fields endpoint for ASM_MailWizzApi PHP-SDK.
 * 
 * @author Serban George Cristian <cristian.serban@mailwizz.com>
 * @link http://www.mailwizz.com/
 * @copyright 2013-2017 http://www.mailwizz.com/
 */
 
 
/**
 * ASM_MailWizzApi_Endpoint_ListFields handles all the API calls for handling the list custom fields.
 * 
 * @author Serban George Cristian <cristian.serban@mailwizz.com>
 * @package ASM_MailWizzApi
 * @subpackage Endpoint
 * @since 1.0
 */
class ASM_MailWizzApi_Endpoint_ListFields extends ASM_MailWizzApi_Base
{
    /**
     * Get fields from a certain mail list
     * 
     * Note, the results returned by this endpoint can be cached.
     * 
     * @param string $listUid
     * @return ASM_MailWizzApi_Http_Response
     */
    public function getFields($listUid)
    {
        $client = new ASM_MailWizzApi_Http_Client(array(
            'method'        => ASM_MailWizzApi_Http_Client::METHOD_GET,
            'url'           => $this->config->getApiUrl(sprintf('lists/%s/fields', $listUid)),
            'paramsGet'     => array(),
            'enableCache'   => true,
        ));
        
        return $response = $client->request();
    }
}