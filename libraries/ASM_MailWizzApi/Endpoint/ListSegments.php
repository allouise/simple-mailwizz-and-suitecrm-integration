<?php
/**
 * This file contains the list segments endpoint for ASM_MailWizzApi PHP-SDK.
 * 
 * @author Serban George Cristian <cristian.serban@mailwizz.com>
 * @link http://www.mailwizz.com/
 * @copyright 2013-2017 http://www.mailwizz.com/
 */
 
 
/**
 * ASM_MailWizzApi_Endpoint_ListSegments handles all the API calls for handling the list segments.
 * 
 * @author Serban George Cristian <cristian.serban@mailwizz.com>
 * @package ASM_MailWizzApi
 * @subpackage Endpoint
 * @since 1.0
 */
class ASM_MailWizzApi_Endpoint_ListSegments extends ASM_MailWizzApi_Base
{
    /**
     * Get segments from a certain mail list
     * 
     * Note, the results returned by this endpoint can be cached.
     * 
     * @param string $listUid
     * @param integer $page
     * @param integer $perPage
     * @return ASM_MailWizzApi_Http_Response
     */
    public function getSegments($listUid, $page = 1, $perPage = 10)
    {
        $client = new ASM_MailWizzApi_Http_Client(array(
            'method'        => ASM_MailWizzApi_Http_Client::METHOD_GET,
            'url'           => $this->config->getApiUrl(sprintf('lists/%s/segments', $listUid)),
            'paramsGet'     => array(
                'page'      => (int)$page, 
                'per_page'  => (int)$perPage
            ),
            'enableCache'   => true,
        ));
        
        return $response = $client->request();
    }
}