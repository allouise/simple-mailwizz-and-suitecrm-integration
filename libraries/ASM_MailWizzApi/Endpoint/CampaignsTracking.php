<?php
/**
 * This file contains the campaigns endpoint for ASM_MailWizzApi PHP-SDK.
 * 
 * @author Serban George Cristian <cristian.serban@mailwizz.com>
 * @link http://www.mailwizz.com/
 * @copyright 2013-2017 http://www.mailwizz.com/
 */
 
 
/**
 * ASM_MailWizzApi_Endpoint_CampaignsTracking handles all the API calls for campaigns.
 * 
 * @author Serban George Cristian <cristian.serban@mailwizz.com>
 * @package ASM_MailWizzApi
 * @subpackage Endpoint
 * @since 1.0
 */
class ASM_MailWizzApi_Endpoint_CampaignsTracking extends ASM_MailWizzApi_Base
{
    /**
     * Track campaign url click for certain subscriber 
     *
     * @param string $campaignUid
     * @param string $subscriberUid
     * @param string $hash
     * @return ASM_MailWizzApi_Http_Response
     */
    public function trackUrl($campaignUid, $subscriberUid, $hash)
    {
        $client = new ASM_MailWizzApi_Http_Client(array(
            'method'    => ASM_MailWizzApi_Http_Client::METHOD_GET,
            'url'       => $this->config->getApiUrl(sprintf('campaigns/%s/track-url/%s/%s', (string)$campaignUid, (string)$subscriberUid, (string)$hash)),
            'paramsGet' => array(),
        ));
        
        return $response = $client->request();
    }

    /**
     * Track campaign open for certain subscriber
     *
     * @param string $campaignUid
     * @param string $subscriberUid
     * @return ASM_MailWizzApi_Http_Response
     */
    public function trackOpening($campaignUid, $subscriberUid)
    {
        $client = new ASM_MailWizzApi_Http_Client(array(
            'method'    => ASM_MailWizzApi_Http_Client::METHOD_GET,
            'url'       => $this->config->getApiUrl(sprintf('campaigns/%s/track-opening/%s', (string)$campaignUid, (string)$subscriberUid)),
            'paramsGet' => array(),
        ));

        return $response = $client->request();
    }

    /**
     * Track campaign unsubscribe for certain subscriber
     *
     * @param string $campaignUid
     * @param string $subscriberUid
     * @param array $data
     * @return ASM_MailWizzApi_Http_Response
     */
    public function trackUnsubscribe($campaignUid, $subscriberUid, array $data = array())
    {
        $client = new ASM_MailWizzApi_Http_Client(array(
            'method'     => ASM_MailWizzApi_Http_Client::METHOD_POST,
            'url'        => $this->config->getApiUrl(sprintf('campaigns/%s/track-unsubscribe/%s', (string)$campaignUid, (string)$subscriberUid)),
            'paramsPost' => $data,
        ));

        return $response = $client->request();
    }
}
