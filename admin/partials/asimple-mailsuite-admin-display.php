<?php

/**
 * Provide a admin area view for the plugin
 *
 * This file is used to markup the admin-facing aspects of the plugin.
 *
 * @link       https://bitbucket.org/allouise/simple-mailwizz-and-suitecrm-integration
 * @since      1.0.0
 *
 * @package    ASimple_MailSuite
 * @subpackage ASimple_MailSuite/admin/partials
 */
?>
<div id="ASimple_MailSuite_loader" class="loader">
    <div class="lds-ellipsis">
        <div></div>
        <div></div>
        <div></div>
        <div></div>
    </div>
</div>
<div class="wrap">
    <h2 class="text-center"><?php echo esc_html( get_admin_page_title() ); ?></h2>
    <?php
    	settings_errors();
    ?>
    <?php if( isset($this->mailwizz_connect_error) && $this->mailwizz_connect_error != "" ): ?>
        <p class="error-block"><?php echo __( 'MailWizz' ).' '.$this->mailwizz_connect_error; ?></p>
    <?php endif; ?>
    <?php if( !isset($this->SuiteCRM_session->id) ): ?>
        <p class="error-block"><?php echo __( 'SuiteCRM' ).' '.__( "Error", $this->ASimple_MailSuite ).": ".$this->SuiteCRM_session->description; ?></p>
    <?php endif; ?>
    
    <form action="options.php" method="post">
        <?php
            settings_fields( $this->plugin_name );
            do_settings_sections( $this->plugin_name );
            submit_button();
        ?>
    </form>
</div>
<!-- <div class="text-center"><small>developed by <a target="_blank" href="http://allysonflores.com">Al Flores</a></small></div> -->