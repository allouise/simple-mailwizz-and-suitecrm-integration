<?php

/**
 * Provide a admin area view for the plugin
 *
 * This file is used to markup the admin-facing aspects of the plugin.
 *
 * @link       https://bitbucket.org/allouise/simple-mailwizz-and-suitecrm-integration
 * @since      1.0.0
 *
 * @package    ASimple_MailSuite
 * @subpackage ASimple_MailSuite/admin/partials
 */
?>
<?php if( $dashboard_widgets === false ){ ?>
<div id="ASimple_MailSuite_loader" class="loader">
    <div class="lds-ellipsis">
        <div></div>
        <div></div>
        <div></div>
        <div></div>
    </div>
</div>
<div id="ASimple_MailSuite" class="wrap">

    <h2 class="text-center"><?php echo esc_html( get_admin_page_title() ); ?></h2><?php } ?>
    <h3><?php echo __( 'MailWizz' ); ?></h3>

    <?php if( !$mailwizz_error ): ?>
        <input type="hidden" name="<?php echo $this->plugin_name."_ajax_nonce"; ?>" value="<?php echo wp_create_nonce($this->plugin_name."_ajax_nonce"); ?>" />
        <table id="<?php echo $this->plugin_name."_view_table"; ?>" class="mailwizz">
            <tbody>
                <tr>
                    <td class="campaigns" data-method="campaigns">
                        <div class="lds-ripple"><div></div><div></div></div>
                        <strong class="count" style="display: none;">0</strong><span class='title'><?php echo __( 'Campaigns', $this->ASimple_MailSuite ); ?></span>
                    </td>
                    <td class="templates" data-method="templates">
                        <div class="lds-ripple"><div></div><div></div></div>
                        <strong class="count" style="display: none;">0</strong><span class='title'><?php echo __( 'Templates', $this->ASimple_MailSuite ); ?></span>
                    </td>
                    <td class="listings" data-method="listings">
                        <div class="lds-ripple"><div></div><div></div></div>
                        <strong class="count" style="display: none;">0</strong><span class='title'><?php echo __( 'Listings', $this->ASimple_MailSuite ); ?></span>
                    </td>
                    <td class="subscribers" data-method="subscribers">
                        <div class="lds-ripple"><div></div><div></div></div>
                        <strong class="count" style="display: none;">0</strong><span class='title'><?php echo __( 'Subscribers', $this->ASimple_MailSuite ); ?></span>
                    </td>
                </tr>
            </tbody>
            <tfoot>
                
            </tfoot>
        </table>
    <?php else: ?>
        <p class="error-block"><?php echo $mailwizz_errordesc; ?></p>
    <?php endif; ?>

    <h3><?php echo __( 'SuiteCRM' ); ?></h3>
    <?php if( !$suitecrm_error ): ?>
        <input type="hidden" name="<?php echo $this->plugin_name."_ajax_nonce"; ?>" value="<?php echo wp_create_nonce($this->plugin_name."_ajax_nonce"); ?>" />
        <table id="<?php echo $this->plugin_name."_view_table"; ?>" class="suitecrm">
            <tbody>
                <tr>
                    <td class="leads" data-method="leads">
                        <div class="lds-ripple"><div></div><div></div></div>
                        <strong class="count" style="display: none;">0</strong><span class='title'><?php echo __( 'Leads', $this->ASimple_MailSuite ) ?></span>
                    </td>
                    <td class="opportunities" data-method="opportunities">
                        <div class="lds-ripple"><div></div><div></div></div>
                        <strong class="count" style="display: none;">0</strong><span class='title'><?php echo __( 'Top 10 Opportunities', $this->ASimple_MailSuite ) ?></span>
                    </td>
                    <td class="calls" data-method="calls">
                        <div class="lds-ripple"><div></div><div></div></div>
                        <strong class="count" style="display: none;">0</strong><span class='title'><?php echo __( 'Calls', $this->ASimple_MailSuite ) ?></span>
                    </td>
                    <td class="meetings" data-method="meetings">
                        <div class="lds-ripple"><div></div><div></div></div>
                        <strong class="count" style="display: none;">0</strong><span class='title'><?php echo __( 'Meetings', $this->ASimple_MailSuite ) ?></span>
                    </td>
                    <?php /* ?><td class="companies" data-method="companies">
                        <div class="lds-ripple"><div></div><div></div></div>
                        <strong class="count" style="display: none;">0</strong><span class='title'>Companies</span>
                    </td><?php */ ?>
                </tr>
            </tbody>
            <tfoot>
                
            </tfoot>
        </table>
    <?php else: ?>
        <p class="error-block"><?php echo $suitecrm_errordesc; ?></p>
    <?php endif; ?>
<?php if( $dashboard_widgets === false ){ ?>
</div>
<div id="pop-up" style="display: none;">
    <div class="popup-container">
        <div class="con">
            <a class="close">&times;</a>
            <h2></h2>
            <div class="pad-con">
                <table>
                    <tr></tr>
                </table>
                <em class="max-desc"></em>
            </div>
        </div>
    </div>
</div>
<!-- <div class="text-center"><small>developed by <a target="_blank" href="http://allysonflores.com">Al Flores</a></small></div> -->
<?php }elseif( $dashboard_widgets === true ){ ?>

    <a href="<?php echo get_admin_url()."admin.php?page=".$this->plugin_name; ?>"><u><?php echo __( "Dashboard" ); ?></u></a> &nbsp; 
    <a href="<?php echo get_admin_url()."admin.php?page=".$this->plugin_name."_settings"; ?>"><u><?php echo __( "Settings" ); ?></u></a>

<?php } ?>