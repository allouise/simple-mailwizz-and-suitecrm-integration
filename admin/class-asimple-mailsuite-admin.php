<?php

/**
 * The admin-specific functionality of the plugin.
 *
 * @link       https://bitbucket.org/allouise/simple-mailwizz-and-suitecrm-integration
 * @since      1.0.0
 *
 * @package    ASimple_MailSuite
 * @subpackage ASimple_MailSuite/admin
 */

/**
 * The admin-specific functionality of the plugin.
 *
 * Defines the plugin name, version, and two examples hooks for how to
 * enqueue the admin-specific stylesheet and JavaScript.
 *
 * @package    ASimple_MailSuite
 * @subpackage ASimple_MailSuite/admin
 * @author     Allyson Flores <elixirlouise@gmail.com>
 */
class ASimple_MailSuite_Admin {

	/**
	 * The ID of this plugin.
	 *
	 * @since    1.0.0
	 * @access   private
	 * @var      string    $ASimple_MailSuite    The ID of this plugin.
	 */
	private $ASimple_MailSuite;

	/**
	 * The version of this plugin.
	 *
	 * @since    1.0.0
	 * @access   private
	 * @var      string    $version    The current version of this plugin.
	 */
	private $version;

	/**
	 * The plugin name handler.
	 *
	 * @since    1.0.0
	 * @access   protected
	 * @var      string    $plugin_name    The plugin name unique slug.
	 */
	protected $plugin_name;

	/**
	 * Mailwizz & SuiteCRM API Vars.
	 *
	 * @since    1.0.0
	 * @access   protected
	 * @var      string    $mailwizz_connect_error    MailWizz Error.
	 * @var      string    $SuiteCRM_session    	  SuiteCRM login Session.
	 */
	protected $mailwizz_connect_error, $SuiteCRM_session;

	/**
	 * Initialize the class and set its properties.
	 *
	 * @since    1.0.0
	 * @param      string    $ASimple_MailSuite       The name of this plugin.
	 * @param      string    $version    The version of this plugin.
	 */
	public function __construct( $ASimple_MailSuite, $version, $plugin_name ) {

		$this->ASimple_MailSuite = $ASimple_MailSuite;
		$this->version = $version;
		$this->plugin_name = $plugin_name;
		$this->mailwizz_connect_error = $this->connect_mailwizz();
		$this->SuiteCRM_session = $this->connect_suiteSuiteCRM();

	}

	/**
	 * Register the stylesheets for the admin area.
	 *
	 * @since    1.0.0
	 */
	public function enqueue_styles() {

		/**
		 * This function is provided for demonstration purposes only.
		 *
		 * An instance of this class should be passed to the run() function
		 * defined in ASimple_MailSuite_Loader as all of the hooks are defined
		 * in that particular class.
		 *
		 * The ASimple_MailSuite_Loader will then create the relationship
		 * between the defined hooks and the functions defined in this
		 * class.
		 */

		wp_enqueue_style( $this->ASimple_MailSuite, plugin_dir_url( __FILE__ ) . 'css/asimple-mailsuite-admin.css', array(), $this->version, 'all' );

	}

	/**
	 * Register the JavaScript for the admin area.
	 *
	 * @since    1.0.0
	 */
	public function enqueue_scripts() {

		/**
		 * This function is provided for demonstration purposes only.
		 *
		 * An instance of this class should be passed to the run() function
		 * defined in ASimple_MailSuite_Loader as all of the hooks are defined
		 * in that particular class.
		 *
		 * The ASimple_MailSuite_Loader will then create the relationship
		 * between the defined hooks and the functions defined in this
		 * class.
		 */

		wp_enqueue_script( $this->plugin_name, plugin_dir_url( __FILE__ ) . 'js/asimple-mailsuite-admin.js', array( 'jquery' ), $this->version, false ); 
		
		wp_register_script( $this->plugin_name.'-mailwizz-ajax', plugin_dir_url( __FILE__ ) . 'js/asimple-mailsuite-admin-mailwizz-ajax.js', array( 'jquery' ), $this->version, true );
		wp_localize_script( $this->plugin_name.'-mailwizz-ajax', $this->plugin_name.'_mailwizz_ajax', array( 'ajaxurl' => admin_url( 'admin-ajax.php' )));

		wp_register_script( $this->plugin_name.'-suitecrm-ajax', plugin_dir_url( __FILE__ ) . 'js/asimple-mailsuite-admin-suitecrm-ajax.js', array( 'jquery' ), $this->version, true );
		wp_localize_script( $this->plugin_name.'-suitecrm-ajax', $this->plugin_name.'_suitecrm_ajax', array( 'ajaxurl' => admin_url( 'admin-ajax.php' )));
		
	}

	/**
	 * Add Settings Links in Plugin list page
	 *
	 * @since    1.0.0
	 */
	public function add_settings_link_plugin( $links ) {
		$links[] = '<a href="' . admin_url( 'admin.php?page=asimple_mailsuite_settings' ) . '">' . __('Settings') . '</a>';
		return $links;
	}
	
	/**
	 * Initiate Dashboard Widgets
	 *
	 * @since    1.0.0
	 */
	public function dashboard_widgets() {
		wp_add_dashboard_widget( $this->plugin_name.'_dashboard_widget', ASimple_MailSuite_Title." ".__( "Dashboard", $this->ASimple_MailSuite ), array( $this, 'dashboard_widgets_display' ) );
	}
	
	/**
	 * Dashboard Widget Display
	 *
	 * @since    1.0.0
	 */
	public function dashboard_widgets_display( $post, $callback_args ) {
		$this->display_dashboard_page(true);
	}

	/**
	 * Connect to Mailwiz.
	 *
	 * @since    1.0.0
	 */
	public function connect_mailwizz() {

		$return = "";
		$url = rtrim(get_option( $this->plugin_name . '_mailwizz_apiurl', '' ), "/");
		$url = ( $url != "" )? $url."/api/index.php" : "";

		try {

		    ASM_MailWizzApi_Autoloader::register();
    		$config = new ASM_MailWizzApi_Config(array(
    		    'apiUrl'        => $url,
    		    'publicKey'     => get_option( $this->plugin_name . '_mailwizz_public_key', '' ),
    		    'privateKey'    => get_option( $this->plugin_name . '_mailwizz_private_key', '' ),

    		    // components
    		    'components' => array(
    		        'cache' => array(
    		            'class'     => 'ASM_MailWizzApi_Cache_File',
    		            'filesPath' => plugin_dir_path( __FILE__ ) . 'libraries/ASM_MailWizzApi/Cache/data/cache', // make sure it is writable by webserver
    		        )
    		    ),
    		));
    		ASM_MailWizzApi_Base::setConfig($config);
    		date_default_timezone_set( ( get_option('timezone_string') != "" )? get_option('timezone_string') : "UTC" );

		} catch (Exception $e) {
		    $return = "Error: " . $e->getMessage();
		}

		return $return;
	}

	/**
	 * Connect to SuiteCRM.
	 *
	 * @since    1.0.0
	 */
	public function connect_suiteSuiteCRM() {

		return $this->call_suiteSuiteCRM( "login", array(
		    "user_auth" => array(
		        "user_name" => get_option( $this->plugin_name . '_suitecrm_username', '' ),
		        "password" => md5( get_option( $this->plugin_name . '_suitecrm_password', '' ) ),
		    )
		) );

	}	

	/**
	 * Unicall for suiteSuiteCRM.
	 *
	 * @since    1.0.0
	 */
	public function call_suiteSuiteCRM($method, $parameters)
	{
	    ob_start();
	    $curl_request = curl_init();
	    $url = rtrim(get_option( $this->plugin_name . '_suitecrm_apiurl' ), "/").'/service/v4_1/rest.php';

	    curl_setopt($curl_request, CURLOPT_URL, $url);
	    curl_setopt($curl_request, CURLOPT_POST, 1);
	    curl_setopt($curl_request, CURLOPT_HTTP_VERSION, CURL_HTTP_VERSION_1_0);
	    curl_setopt($curl_request, CURLOPT_HEADER, 1);
	    curl_setopt($curl_request, CURLOPT_SSL_VERIFYPEER, 0);
	    curl_setopt($curl_request, CURLOPT_RETURNTRANSFER, 1);
	    curl_setopt($curl_request, CURLOPT_FOLLOWLOCATION, 0);

	    $jsonEncodedData = json_encode($parameters);

	    $post = array(
	        "method" => $method,
	        "input_type" => "JSON",
	        "response_type" => "JSON",
	        "rest_data" => $jsonEncodedData
	    );

	    curl_setopt($curl_request, CURLOPT_POSTFIELDS, $post);
	    $result = curl_exec($curl_request);
	    curl_close($curl_request);

	    $result = explode("\r\n\r\n", $result, 2);
	    $response = json_decode($result[1]);
	    ob_end_flush();

	    return $response;
	}

	/**
	 * Add new page for the Admin Area.
	 *
	 * @since    1.0.0
	 */
	public function add_admin_menu_page() {
		
		add_menu_page( ASimple_MailSuite_Title." ".__( "Dashboard", $this->ASimple_MailSuite ), ASimple_MailSuite_Title." ".__( "Dashboard", $this->ASimple_MailSuite ), "manage_options", $this->plugin_name, array( $this, 'display_dashboard_page' ), 'dashicons-email-alt2', 80 );
		add_submenu_page( $this->plugin_name, ASimple_MailSuite_Title." ".__( "Settings", $this->ASimple_MailSuite ), __( "Settings", $this->ASimple_MailSuite ), "manage_options", $this->plugin_name.'_settings', array( $this, 'display_settings_page' ) );
		
	}

	/**
	 * Dashboard page for the Admin Area.
	 *
	 * @since    1.0.0
	 */
	public function display_dashboard_page( $dashboard_widgets = "" ) {
		$dashboard_widgets = ( $dashboard_widgets == true )? true : false;
		$mailwizz_error = true;
		$mailwizz_errordesc = __( 'Please setup your correct MailWizz API Details', $this->ASimple_MailSuite );
		$suitecrm_error = true;
		$suitecrm_errordesc = __( 'Please setup your correct SuiteCRM Login Details', $this->ASimple_MailSuite );

		if( $this->mailwizz_connect_error == "" ){
			$mailwizz_error = false;
			wp_enqueue_script( $this->plugin_name.'-mailwizz-ajax' );
		}

		if( isset($this->SuiteCRM_session->id) ){
			$suitecrm_error = false;
			wp_enqueue_script( $this->plugin_name.'-suitecrm-ajax' );
		}

	    require_once plugin_dir_path( __FILE__ ). 'partials/asimple-mailsuite-dashboard-display.php';

	}

	/**
	 * Load Tables | Ajax Called - Die after init
	 *
	 * @since    1.0.0
	 */
	public function load_tables() {

		$return = array(
			'success' => false,
			'errors' => array()
		);

		$mailwizz_methods = array('listingsubs','listings','campaigns','subscribers','templates');
		$suitecrm_methods = array('leads','opportunities','calls','meetings','companies');
		$suites = array('mailwizz','suitecrm');

		if ( 
			!wp_verify_nonce( $_POST['nonce'], $this->plugin_name."_ajax_nonce" ) || 
			( !in_array($_POST['method'], $mailwizz_methods) && !in_array($_POST['method'], $suitecrm_methods) ) || 
			( $_POST['suite'] == 'mailwizz' && !in_array($_POST['method'], $mailwizz_methods ) ) ||
			( $_POST['suite'] == 'suitecrm' && !in_array($_POST['method'], $suitecrm_methods ) ) ||
			!in_array($_POST['suite'], $suites) 
		) {
			$return['errors'][] = __( "Invalid Request please refresh this page. If problem persist contact administrator." );
			echo json_encode($return);
			wp_die();
		}
		
		switch ( $_POST['suite'] ) {
			case 'mailwizz':
				$return = $this->get_mailwizz_content($_POST['method']);
				break;
			case 'suitecrm':
				$return = $this->get_suitecrm_content($_POST['method']);
				break;
			default:
				# code...
				break;
		}
		
		echo json_encode($return);
		wp_die();
	}

	/**
	 * Get Ajax Content for MailWizz.
	 *
	 * @since    1.0.0
	 * @var      string    $method    The method to get in Mailwizz API.
	 */
	public function get_mailwizz_content($method) {

		$return = array(
			'success' => false,
			'errors' => array(),
			'method_return' => array(),
			'max_desc' => sprintf( 
				wp_kses( __( 'Viewing Top 10 Data', $this->ASimple_MailSuite ).', <a target="%s" href="%s"><u><strong>'.__( 'View More', $this->ASimple_MailSuite ).'</strong></u></a>' , array( 'a' => array( 'href' => array(), 'target' => array() ), 'u' => array(), 'strong' => array() ) ), 
				'_blank',
				esc_url( get_option( $this->plugin_name . '_mailwizz_apiurl', '#' ) ) 
			)
		);
		
		if( $this->mailwizz_connect_error == "" && isset($method) ){

			$limit = /*isset($_GET['limit'])? sanitize_text_field($_GET['limit']) :*/ 10;
			$page = /*isset($_GET['page'])? sanitize_text_field($_GET['page']) :*/ 1;

			switch ( $method ) {
				case 'listings':
				case 'listingsubs':

					# List
					$lists = new ASM_MailWizzApi_Endpoint_Lists();
					$getLists = $lists->getLists( $page, $limit );
					if( $getLists->body['status'] == 'success' ){

						$return['method_return']['listings'] = array( 
							'count' => (int)$getLists->body['data']['count'],
							'data' => $getLists->body['data']['records'],
							'header' => array(
								"list_uid" => __( "Unique ID", $this->ASimple_MailSuite ),
								"display_name" => __( "Visible Name", $this->ASimple_MailSuite ),
								"name" => __( "Name", $this->ASimple_MailSuite ),
								"description" => __( "Description", $this->ASimple_MailSuite )
							)
						);

						if( $method == 'listingsubs' ){
							
							$return['method_return']['subscribers'] = array( 
								'count' => 0,
								'data' => null,
								'header' => array(
									"FNAME" => __( "First Name", $this->ASimple_MailSuite ),
									"LNAME" => __( "Last Name", $this->ASimple_MailSuite ),
									"EMAIL" => __( "Email", $this->ASimple_MailSuite ),
									"AGE" => __( "Age", $this->ASimple_MailSuite ),
									"date_added" => __( "Date Added", $this->ASimple_MailSuite ),
									"status" => __( "Status", $this->ASimple_MailSuite )
								)
							);
							
							if( $return['method_return']['listings']['count'] <= 0 ){
								$return['success'] = true;
								break;
							}

							# Subscribers
							$subscribers = new ASM_MailWizzApi_Endpoint_ListSubscribers();
							#Get Lists & their subscribers
							for ($lcount = 0; $lcount <= $return['method_return']['listings']['count']; $lcount++) { 

								$listid = $return['method_return']['listings']['data'][$lcount]['general']['list_uid'];
								$getSubscribers = $subscribers->getSubscribers( $listid, $page, $limit );

								if( $getSubscribers->body['status'] == 'success' ){
									$return['method_return']['subscribers']['count'] += (int) $getSubscribers->body['data']['count'];
									$return['method_return']['subscribers']['data'][$listid] =  $getSubscribers->body['data']['records'];
									$return['success'] = true;
								}else{
									$return['errors']['subscribers'] = "<strong>".__( "Subscribers", $this->ASimple_MailSuite )." ".__( "Error", $this->ASimple_MailSuite )."</strong>: " . $getSubscribers->body['error'];
									break;
								}
								
							}

						}else{ break; }
					}else{
						$return['errors']['listings'] = "<strong>".__( "Listings", $this->ASimple_MailSuite )." ".__( "Error", $this->ASimple_MailSuite )."</strong>: " . $getLists->body['error'];

						if( $method == 'listingsubs' )
							$return['errors']['subscribers'] .= "<strong>".__( "Subscribers", $this->ASimple_MailSuite )." ".__( "Error", $this->ASimple_MailSuite )."</strong>: " . $getLists->body['error'];
					}

				break;

				case 'campaigns':

					$campaigns = new ASM_MailWizzApi_Endpoint_Campaigns();
					$getCampaigns = $campaigns->getCampaigns( $page, $limit );
					# Campaigns
					if( $getCampaigns->body['status'] == 'success' && empty($return['errors']) ){
						$return['method_return']['campaigns'] = array( 
							'count' => (int)$getCampaigns->body['data']['count'],
							'data' => $getCampaigns->body['data']['records'],
							'header' => array(
								"campaign_uid" => __( "Campaign ID", $this->ASimple_MailSuite ),
								"name" => __( "Name", $this->ASimple_MailSuite ),
								"status" => __( "Status", $this->ASimple_MailSuite )
							)
						);
						$return['success'] = true;
					}elseif( empty($return['errors']) ){
						$return['errors']['campaigns'] = "<strong>".__( "Campaigns", $this->ASimple_MailSuite )." ".__( "Error", $this->ASimple_MailSuite )."</strong>: " . $getCampaigns->body['error'];
					}

				break;
				
				case 'templates':

					$templates = new ASM_MailWizzApi_Endpoint_Templates();
					$getTemplates = $templates->getTemplates( $page, $limit );
					# Templates
					if( $getTemplates->body['status'] == 'success' && empty($return['errors']) ){
						$return['method_return']['templates'] = array( 
							'count' => (int)$getTemplates->body['data']['count'],
							'data' => $getTemplates->body['data']['records'],
							'header' => array(
								"template_uid" => __( "Template ID", $this->ASimple_MailSuite ),
								"name" => __( "Name", $this->ASimple_MailSuite ),
								"screenshot" => __( "Screenshot", $this->ASimple_MailSuite )
							)
						);
						$return['success'] = true;
					}elseif( empty($return['errors']) ){
						$return['errors']['templates'] = "<strong>".__( "Templates", $this->ASimple_MailSuite )." ".__( "Error", $this->ASimple_MailSuite )."</strong>: " . $getTemplates->body['error'];
					}

				break;
					
				
				default:
				break;
			}
			
		}

		return $return;
	}

	/**
	 * Get Ajax Content for MailWizz.
	 *
	 * @since    1.0.0
	 * @var      string    $method    The method to get in Mailwizz API.
	 */
	public function get_suitecrm_content($method) {

		$return = array(
			'success' => false,
			'errors' => array(),
			'method_return' => array(),
			'max_desc' => sprintf( 
				wp_kses( __( 'Viewing Top 10 Data', $this->ASimple_MailSuite ).', <a target="%s" href="%s"><u><strong>'.__( 'View More', $this->ASimple_MailSuite ).'</strong></u></a>' , array( 'a' => array( 'href' => array(), 'target' => array() ), 'u' => array(), 'strong' => array() ) ), 
				'_blank',
				esc_url( get_option( $this->plugin_name . '_suitecrm_apiurl', '#' ) ) 
			)
		);
		
		if( isset($this->SuiteCRM_session->id) && isset($method) ){
			$sessID = $this->SuiteCRM_session->id;
			$limit = /*isset($_GET['limit'])? sanitize_text_field($_GET['limit']) :*/ 10;
			$page = /*isset($_GET['page'])? sanitize_text_field($_GET['page']) :*/ 1;
			
			switch ($method) {
				case 'leads':
					$header = array(
						"name"	=> __( "Name", $this->ASimple_MailSuite ),
						"status" => __( "Status", $this->ASimple_MailSuite ),
						"account_name" => __( "Account Name", $this->ASimple_MailSuite ),
						"phone_work" => __( "Office Phone", $this->ASimple_MailSuite ),
						"email" => __( "Primary Email", $this->ASimple_MailSuite ),
						"email1" => __( "Email 1", $this->ASimple_MailSuite ),
						"email2" => __( "Email 2", $this->ASimple_MailSuite ),
						"modified_by_name" => __( "User", $this->ASimple_MailSuite ),
						"date_entered" => __( "Date Created", $this->ASimple_MailSuite )
					);
					break;
				case 'opportunities':
					$header = array(
						"name"	=> __( "Name", $this->ASimple_MailSuite ),
						"account_name" => __( "Account Name", $this->ASimple_MailSuite ),
						"sales_stage" => __( "Sales Stage", $this->ASimple_MailSuite ),
						"amount" => __( "Amount", $this->ASimple_MailSuite ),
						"date_closed" => __( "Close", $this->ASimple_MailSuite ),
						"assigned_user_name" => __( "User", $this->ASimple_MailSuite ),
						"date_entered" => __( "Date Created", $this->ASimple_MailSuite )
					);
					break;
				case 'calls':
					$header = array(
						"direction"	=> __( "Direction", $this->ASimple_MailSuite ),
						"name" => __( "Subject", $this->ASimple_MailSuite ),
						"parent_name" => __( "Related To", $this->ASimple_MailSuite ),
						"date_start" => __( "Start Date", $this->ASimple_MailSuite ),
						"assigned_user_name" => __( "Assigned to", $this->ASimple_MailSuite ),
						"date_entered" => __( "Date Created", $this->ASimple_MailSuite )
					);
					break;
				case 'meetings':
					$header = array(
						"name"	=> __( "Subject", $this->ASimple_MailSuite ),
						"contact_name" => __( "Contact", $this->ASimple_MailSuite ),
						"parent_name" => __( "Related To", $this->ASimple_MailSuite ),
						"date_start" => __( "Start Date", $this->ASimple_MailSuite ),
						"assigned_user_name" => __( "Assigned User", $this->ASimple_MailSuite ),
						"date_entered" => __( "Date Created", $this->ASimple_MailSuite )
					);
					break;
				default:
					$header = "";
					break;
			}

			$get_entries = array(
				'session' => $sessID,
				'module_name' => ucfirst($method),
				'query' => "",
				'order_by' => ( $method == "opportunities" )? "amount_usdollar DESC" : "",
				'offset' => $page - 1,
				'select_fields' => array(),
				'link_name_to_fields_array' => array(),
				'max_results' => $limit,
				'deleted' => 0,
			);
			$entries = $this->call_suiteSuiteCRM("get_entry_list", $get_entries);
			$return['method_return'] = $entries;
			$return['header'] = $header;

			if( isset($entries->total_count) ){
				$return['success'] = true;
			}else{
				$return['errors'][] = ucfirst($method). " ".__( "Error", $this->ASimple_MailSuite ).": " .$entries->description;
			}
			
			
		}

		return $return;
	}

	/**
	 * Settings page for the Admin Area.
	 *
	 * @since    1.0.0
	 */
	public function display_settings_page() {

	    require_once plugin_dir_path( __FILE__ ). 'partials/asimple-mailsuite-admin-display.php';

	}

	/**
	 * Register Settings for the plugin.
	 *
	 * @since    1.0.0
	 */
	public function register_setting() {
		$args = array(
			'type' => 'string', 
			'sanitize_callback' => 'sanitize_text_field',
			'default' => NULL,
		);

		add_settings_section( $this->plugin_name . "_mailwizz_api", "", array( $this, $this->plugin_name . "_mailwizz_api" ), $this->plugin_name );

			#MailWizz API URL
			add_settings_field( 
				$this->plugin_name . '_mailwizz_apiurl', 
				__( 'MailWizz Login URL', $this->ASimple_MailSuite ), 
				array( $this, $this->plugin_name . '_mailwizz_api_apiurl' ),  
				$this->plugin_name, 
				$this->plugin_name . '_mailwizz_api',
				array( 'label_for' => $this->plugin_name . '_mailwizz_apiurl' ) 
			);

			#MailWizz Public Key
			add_settings_field( 
				$this->plugin_name . '_mailwizz_public_key', 
				__( 'Public Key', $this->ASimple_MailSuite ), 
				array( $this, $this->plugin_name . '_mailwizz_api_public_key' ),  
				$this->plugin_name, 
				$this->plugin_name . '_mailwizz_api',
				array( 'label_for' => $this->plugin_name . '_mailwizz_public_key' ) 
			);
			
			#MailWizz Private Key
			add_settings_field( 
				$this->plugin_name . '_mailwizz_private_key', 
				__( 'Private Key', $this->ASimple_MailSuite ), 
				array( $this, $this->plugin_name . '_mailwizz_api_private_key' ),  
				$this->plugin_name,
				$this->plugin_name . '_mailwizz_api',
				array( 'label_for' => $this->plugin_name . '_mailwizz_private_key' ) 
			);

			register_setting( $this->plugin_name, $this->plugin_name . '_mailwizz_apiurl', $args );
			register_setting( $this->plugin_name, $this->plugin_name . '_mailwizz_public_key', $args );
			register_setting( $this->plugin_name, $this->plugin_name . '_mailwizz_private_key', $args );

		add_settings_section( $this->plugin_name . "_suitecrm_api", "", array( $this, $this->plugin_name . "_suitecrm_api" ), $this->plugin_name );

			#SuiteCRM URL
			add_settings_field( 
				$this->plugin_name . '_suitecrm_apiurl', 
				__( 'SuiteCRM Login URL', $this->ASimple_MailSuite ), 
				array( $this, $this->plugin_name . '_suitecrm_api_apiurl' ),  
				$this->plugin_name, 
				$this->plugin_name . '_suitecrm_api',
				array( 'label_for' => $this->plugin_name . '_suitecrm_apiurl' ) 
			);

			#SuiteCRM Public Key
			add_settings_field( 
				$this->plugin_name . '_suitecrm_username', 
				__( 'SuiteCRM Username', $this->ASimple_MailSuite ), 
				array( $this, $this->plugin_name . '_suitecrm_api_username' ),  
				$this->plugin_name, 
				$this->plugin_name . '_suitecrm_api',
				array( 'label_for' => $this->plugin_name . '_suitecrm_username' ) 
			);
			
			#SuiteCRM Private Key
			add_settings_field( 
				$this->plugin_name . '_suitecrm_password', 
				__( 'SuiteCRM Password', $this->ASimple_MailSuite ), 
				array( $this, $this->plugin_name . '_suitecrm_api_password' ),  
				$this->plugin_name,
				$this->plugin_name . '_suitecrm_api',
				array( 'label_for' => $this->plugin_name . '_suitecrm_password' ) 
			);

			register_setting( $this->plugin_name, $this->plugin_name . '_suitecrm_apiurl', $args );
			register_setting( $this->plugin_name, $this->plugin_name . '_suitecrm_username', $args );
			register_setting( $this->plugin_name, $this->plugin_name . '_suitecrm_password', $args );
	}

	/**
	 * Render the treshold Mailwizz Section
	 *
	 * @since  1.0.0
	 */
	public function asimple_mailsuite_mailwizz_api() {
		echo "<h3 class='b-b'>".__( "MailWizz API Credentials", $this->ASimple_MailSuite )."</h3>";
	}

	/**
	 * Render the treshold MailWizz API URL input
	 *
	 * @since  1.0.0
	 */
	public function asimple_mailsuite_mailwizz_api_apiurl() {
		$mailwizz_apiurl = get_option( $this->plugin_name . '_mailwizz_apiurl', '' );
		?>
		<input type="text" name="<?php echo $this->plugin_name . "_mailwizz_apiurl"; ?>" id="<?php echo $this->plugin_name . "_mailwizz_apiurl"; ?>" value="<?php echo $mailwizz_apiurl; ?>">
		<?php
	}

	/**
	 * Render the treshold MailWizz Public Key input
	 *
	 * @since  1.0.0
	 */
	public function asimple_mailsuite_mailwizz_api_public_key() {
		$mailwizz_public_key = get_option( $this->plugin_name . '_mailwizz_public_key', '' );
		?>
		<input type="text" name="<?php echo $this->plugin_name . "_mailwizz_public_key"; ?>" id="<?php echo $this->plugin_name . "_mailwizz_public_key"; ?>" value="<?php echo $mailwizz_public_key; ?>">
		<?php
	}

	/**
	 * Render the treshold MailWizz Private Key input
	 *
	 * @since  1.0.0
	 */
	public function asimple_mailsuite_mailwizz_api_private_key() {
		$mailwizz_private_key = get_option( $this->plugin_name . '_mailwizz_private_key', '' );
		?>
		<input type="password" autocomplete="off" name="<?php echo $this->plugin_name . "_mailwizz_private_key"; ?>" id="<?php echo $this->plugin_name . "_mailwizz_private_key"; ?>" value="<?php echo $mailwizz_private_key; ?>">
		<?php
	}

	/**
	 * Render the treshold SuiteCRM Section
	 *
	 * @since  1.0.0
	 */
	public function asimple_mailsuite_suitecrm_api() {
		echo "<h3 class='b-b'>".__( "SuiteCRM API Credentials", $this->ASimple_MailSuite )."</h3>";
	}

	/**
	 * Render the treshold SuiteCRM API URL input
	 *
	 * @since  1.0.0
	 */
	public function asimple_mailsuite_suitecrm_api_apiurl() {
		$suitecrm_apiurl = get_option( $this->plugin_name . '_suitecrm_apiurl', '' );
		?>
		<input type="text" name="<?php echo $this->plugin_name . "_suitecrm_apiurl"; ?>" id="<?php echo $this->plugin_name . "_suitecrm_apiurl"; ?>" value="<?php echo $suitecrm_apiurl; ?>">
		<?php
	}

	/**
	 * Render the treshold SuiteCRM Username input
	 *
	 * @since  1.0.0
	 */
	public function asimple_mailsuite_suitecrm_api_username() {
		$suitecrm_username = get_option( $this->plugin_name . '_suitecrm_username', '' );
		?>
		<input type="text" name="<?php echo $this->plugin_name . "_suitecrm_username"; ?>" id="<?php echo $this->plugin_name . "_suitecrm_username"; ?>" value="<?php echo $suitecrm_username; ?>">
		<?php
	}

	/**
	 * Render the treshold SuiteCRM Password input
	 *
	 * @since  1.0.0
	 */
	public function asimple_mailsuite_suitecrm_api_password() {
		$suitecrm_password = get_option( $this->plugin_name . '_suitecrm_password', '' );
		?>
		<input type="password" autocomplete="off" name="<?php echo $this->plugin_name . "_suitecrm_password"; ?>" id="<?php echo $this->plugin_name . "_suitecrm_password"; ?>" value="<?php echo $suitecrm_password; ?>">
		<?php
	}

}
