(function( $ ) {
	'use strict';

	/**
	 * All of the code for your admin-facing JavaScript source
	 * should reside in this file.
	 *
	 * Note: It has been assumed you will write jQuery code here, so the
	 * $ function reference has been prepared for usage within the scope
	 * of this function.
	 *
	 * This enables you to define handlers, for when the DOM is ready:
	 *
	 * $(function() {
	 *
	 * });
	 *
	 * When the window is loaded:
	 *
	 * $( window ).load(function() {
	 *
	 * });
	 *
	 * ...and/or other possibilities.
	 *
	 * Ideally, it is not considered best practise to attach more than a
	 * single DOM-ready or window-load handler for a particular page.
	 * Although scripts in the WordPress core, Plugins and Themes may be
	 * practising this, we should strive to set a better example in our own work.
	 */

	$(document).ready(function(){

		var _popup = $('#pop-up'),
			_table = $('#asimple_mailsuite_view_table.mailwizz'),
			nonce = $('[name="asimple_mailsuite_ajax_nonce"]').val(),
			errors = '',
			returns = {
				listings : [],
				campaigns : [],
				templates : [],
				subscribers : []
			},
			loaded = 0,
			max_desc = "";

		_table.removeClass('has-error');
		_table.find('.lds-ripple').show();

		/* INIT: campaigns */
		$.ajax({
			type : "post",
			dataType : "json",
			url : asimple_mailsuite_mailwizz_ajax.ajaxurl,
			data : {action: "asimple_mailsuite_admin_ajax", nonce: nonce, method: "campaigns", suite: "mailwizz"},
			success: function(response) {
				if(response.success == true) {
					_table.find('.campaigns .lds-ripple').hide().siblings('.count').html(response.method_return.campaigns.count).show();
					returns['campaigns'] = ( response.method_return.campaigns.count > 0 )? response.method_return.campaigns.data : [];
					returns['campaigns']['header'] = response.method_return.campaigns.header;
					max_desc = response.max_desc;
					loaded += 1;
				} else {
					$.each(response.errors, function(i,e){
						_table.find('.'+i).addClass('has-error');
						errors = "<tr><td colspan='4'>"+e+"</td></tr>";
					});
					_table.find('tfoot').append(errors);
					_table.find('.campaigns').addClass('has-error').find('.lds-ripple').hide().siblings('.campaigns .count').html(0).show();
					loaded += 1;
				}
			}
		});

		/* Templates */
		$.ajax({
			type : "post",
			dataType : "json",
			url : asimple_mailsuite_mailwizz_ajax.ajaxurl,
			data : {action: "asimple_mailsuite_admin_ajax", nonce: nonce, method: "templates", suite: "mailwizz"},
			success: function(response) {
				if(response.success == true) {
					_table.find('.templates .lds-ripple').hide().siblings('.count').html(response.method_return.templates.count).show();
					returns['templates'] = ( response.method_return.templates.count > 0 )? response.method_return.templates.data : [];
					returns['templates']['header'] = response.method_return.templates.header;
					max_desc = response.max_desc;
					loaded += 1;
				} else {
					$.each(response.errors, function(i,e){
						_table.find('.'+i).addClass('has-error');
						errors = "<tr><td colspan='4'>"+e+"</td></tr>";
					});
					_table.find('tfoot').append(errors);
					_table.find('.templates').addClass('has-error').find('.lds-ripple').hide().siblings('.templates .count').html(0).show();
					loaded += 1;
				}
			}
		});

		/* Listing & Subs */
		$.ajax({
			type : "post",
			dataType : "json",
			url : asimple_mailsuite_mailwizz_ajax.ajaxurl,
			data : {action: "asimple_mailsuite_admin_ajax", nonce: nonce, method: "listingsubs", suite: "mailwizz"},
			success: function(response) {
				if(response.success == true) {
					_table.find('.listings .lds-ripple').hide().siblings('.count').html(response.method_return.listings.count).show();
					_table.find('.subscribers .lds-ripple').hide().siblings('.count').html(response.method_return.subscribers.count).show();
					returns['listings'] = ( response.method_return.listings.count > 0 )? response.method_return.listings.data : [];
					returns['listings']['header'] = response.method_return.listings.header;
					returns['subscribers'] = ( response.method_return.subscribers.count > 0 )? response.method_return.subscribers.data : [];
					returns['subscribers']['header'] = response.method_return.subscribers.header;
					loaded += 2;
					max_desc = response.max_desc;
				} else {
					$.each(response.errors, function(i,e){
						_table.find('.'+i).addClass('has-error');
						errors = "<tr><td colspan='4'>"+e+"</td></tr>";
					});
					_table.find('tfoot').append(errors);
					_table.find('.listings').addClass('has-error').find('.lds-ripple').hide().siblings('.listings .count').html(0).show();
					_table.find('.subscribers').addClass('has-error').find('.lds-ripple').hide().siblings('.subscribers .count').html(0).show();
					loaded += 2;
				}
			}
		});

		_table.find('[data-method] .count').on('click',function(){
			var _method = $(this).parents('td').attr('data-method'),
				_col = $('.'+_method);

			if( !_col.hasClass('has-error') && loaded == Object.keys(returns).length ){

				_table = mailwizz_format_content( returns[_method], _method );
				_popup.find('h2').html(_col.find('.title').text());
				_popup.find('table').html(_table);
				_popup.find('.max-desc').html(max_desc);
				_popup.fadeIn();
			}
			
		});

		_popup.find('.close').on('click',function(){
			_popup.find('h2').html("");
			_popup.find('table').html("");
			_popup.find('.max-desc').html("");
			_popup.hide();
		});


		function mailwizz_format_content( data_array, _method ){

			var _return = "";

			$.each(data_array.header, function(i,e){
				_return += "<th>"+e+"</th>";
			});

			if( _return != "" ) _return = "<thead><tr>"+_return+"</tr></thead>";

			$.each(data_array, function(q,w){
				

				if( _method == 'subscribers' ){
					$.each(w, function(n,m){
						_return += "<tr>";
						$.each(data_array.header, function(x,d){
							if( q != "header" ){
								_return += "<td>"+ m[x] +"</td>";
							}
						});
						_return += "</tr>";
					});
				}else{
					_return += "<tr>";

					$.each(data_array.header, function(x,d){
						if( q != "header" ){
							if( _method == 'listings' ){
								_return += "<td>"+( x == 'screenshot' && w['general'][x] != null? "<img src='"+w['general'][x]+"'/>" : w['general'][x] ) +"</td>";
							}else{
								_return += "<td>"+( x == 'screenshot' && w[x] != null? "<img src='"+w[x]+"'/>" : w[x] ) +"</td>";
							}
						}
					});

					_return += "</tr>";
				}

				
			});

			return _return;

		}

	});

})( jQuery );
