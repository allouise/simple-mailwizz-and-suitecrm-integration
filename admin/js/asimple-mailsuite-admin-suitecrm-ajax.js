(function( $ ) {
	'use strict';

	/**
	 * All of the code for your admin-facing JavaScript source
	 * should reside in this file.
	 *
	 * Note: It has been assumed you will write jQuery code here, so the
	 * $ function reference has been prepared for usage within the scope
	 * of this function.
	 *
	 * This enables you to define handlers, for when the DOM is ready:
	 *
	 * $(function() {
	 *
	 * });
	 *
	 * When the window is loaded:
	 *
	 * $( window ).load(function() {
	 *
	 * });
	 *
	 * ...and/or other possibilities.
	 *
	 * Ideally, it is not considered best practise to attach more than a
	 * single DOM-ready or window-load handler for a particular page.
	 * Although scripts in the WordPress core, Plugins and Themes may be
	 * practising this, we should strive to set a better example in our own work.
	 */

	$(document).ready(function(){

		var _popup = $('#pop-up'),
			_table = $('#asimple_mailsuite_view_table.suitecrm'),
			nonce = $('[name="asimple_mailsuite_ajax_nonce"]').val(),
			errors = '',
			returns = {
				leads : [],
				opportunities : [],
				calls : [],
				meetings : []/*,
				companies : []*/
			},
			loaded = 0,
			max_desc = "";

		_table.removeClass('has-error');
		_table.find('.lds-ripple').show();

		/* leads */
		$.ajax({
			type : "post",
			dataType : "json",
			url : asimple_mailsuite_suitecrm_ajax.ajaxurl,
			data : {action: "asimple_mailsuite_admin_ajax", nonce: nonce, method: "leads", suite: "suitecrm"},
			success: function(response) {
				if(response.success == true) {
					_table.find('.leads .lds-ripple').hide().siblings('.count').html(response.method_return.total_count).show();
					returns['leads'] = ( response.method_return.total_count > 0 )? response.method_return.entry_list : [];
					returns['leads']['header'] = response.header;
					loaded += 1;
					max_desc = response.max_desc;
				} else {
					$.each(response.errors, function(i,e){
						_table.find('.'+i).addClass('has-error');
						errors = "<tr><td colspan='4'>"+e+"</td></tr>";
					});
					_table.find('tfoot').append(errors);
					_table.find('.leads').addClass('has-error').find('.lds-ripple').hide().siblings('.leads .count').html(0).show();
					loaded += 1;
				}
			}
		});

		/* opportunities */
		$.ajax({
			type : "post",
			dataType : "json",
			url : asimple_mailsuite_suitecrm_ajax.ajaxurl,
			data : {action: "asimple_mailsuite_admin_ajax", nonce: nonce, method: "opportunities", suite: "suitecrm"},
			success: function(response) {
				if(response.success == true) {
					_table.find('.opportunities .lds-ripple').hide().siblings('.count').html(response.method_return.result_count).show();
					returns['opportunities'] = ( response.method_return.result_count > 0 )? response.method_return.entry_list : [];
					returns['opportunities']['header'] = response.header;
					loaded += 1;
					max_desc = response.max_desc;
				} else {
					$.each(response.errors, function(i,e){
						_table.find('.'+i).addClass('has-error');
						errors = "<tr><td colspan='4'>"+e+"</td></tr>";
					});
					_table.find('tfoot').append(errors);
					_table.find('.opportunities').addClass('has-error').find('.lds-ripple').hide().siblings('.opportunities .count').html(0).show();
					loaded += 1;
				}
			}
		});

		/* calls */
		$.ajax({
			type : "post",
			dataType : "json",
			url : asimple_mailsuite_suitecrm_ajax.ajaxurl,
			data : {action: "asimple_mailsuite_admin_ajax", nonce: nonce, method: "calls", suite: "suitecrm"},
			success: function(response) {
				if(response.success == true) {
					_table.find('.calls .lds-ripple').hide().siblings('.count').html(response.method_return.total_count).show();
					returns['calls'] = ( response.method_return.total_count > 0 )? response.method_return.entry_list : [];
					returns['calls']['header'] = response.header;
					loaded += 1;
					max_desc = response.max_desc;
				} else {
					$.each(response.errors, function(i,e){
						_table.find('.'+i).addClass('has-error');
						errors = "<tr><td colspan='4'>"+e+"</td></tr>";
					});
					_table.find('tfoot').append(errors);
					_table.find('.calls').addClass('has-error').find('.lds-ripple').hide().siblings('.calls .count').html(0).show();
					loaded += 1;
				}
			}
		});

		/* meetings */
		$.ajax({
			type : "post",
			dataType : "json",
			url : asimple_mailsuite_suitecrm_ajax.ajaxurl,
			data : {action: "asimple_mailsuite_admin_ajax", nonce: nonce, method: "meetings", suite: "suitecrm"},
			success: function(response) {
				if(response.success == true) {
					_table.find('.meetings .lds-ripple').hide().siblings('.count').html(response.method_return.total_count).show();
					returns['meetings'] = ( response.method_return.total_count > 0 )? response.method_return.entry_list : [];
					returns['meetings']['header'] = response.header;
					loaded += 1;
					max_desc = response.max_desc;
				} else {
					$.each(response.errors, function(i,e){
						_table.find('.'+i).addClass('has-error');
						errors = "<tr><td colspan='4'>"+e+"</td></tr>";
					});
					_table.find('tfoot').append(errors);
					_table.find('.meetings').addClass('has-error').find('.lds-ripple').hide().siblings('.meetings .count').html(0).show();
					loaded += 1;
				}
			}
		});

		/* companies */
		/*$.ajax({
			type : "post",
			dataType : "json",
			url : asimple_mailsuite_suitecrm_ajax.ajaxurl,
			data : {action: "asimple_mailsuite_admin_ajax", nonce: nonce, method: "companies", suite: "suitecrm"},
			success: function(response) {
				if(response.success == true) {
					_table.find('.companies .lds-ripple').hide().siblings('.count').html(response.method_return.total_count).show();
					returns['companies'] = ( response.method_return.total_count > 0 )? response.method_return.entry_list : [];
					returns['companies']['header'] = response.header;
				} else {
					$.each(response.errors, function(i,e){
						_table.find('.'+i).addClass('has-error');
						errors = "<tr><td colspan='4'>"+e+"</td></tr>";
					});
					_table.find('tfoot').append(errors);
					_table.find('.companies').addClass('has-error').find('.lds-ripple').hide().siblings('.companies .count').html(0).show();
				}
			}
		});*/


		_table.find('[data-method] .count').on('click',function(){
			var _method = $(this).parents('td').attr('data-method'),
				_col = $('.'+_method);

			if( !_col.hasClass('has-error') && returns[_method].length > 0 && loaded == Object.keys(returns).length ){
				
				_table = suitecrm_format_content( returns[_method], _method );
				_popup.find('h2').html(_col.find('.title').text());
				_popup.find('table').html(_table);
				_popup.find('.max-desc').html(max_desc);
				_popup.fadeIn();
			}
			
		});

		_popup.find('.close').on('click',function(){
			_popup.find('h2').html("");
			_popup.find('table').html("");
			_popup.find('.max-desc').html("");
			_popup.hide();
		});


		function suitecrm_format_content( data_array, _method ){

			var _return = "";

			$.each(data_array.header, function(i,e){
				_return += "<th>"+e+"</th>";
			});

			if( _return != "" ) _return = "<thead><tr>"+_return+"</tr></thead>";

			$.each(data_array, function(q,w){
				_return += "<tr>";
				$.each(data_array.header, function(x,d){
					if( q != "header" )
					_return += "<td>"+w['name_value_list'][x]['value']+"</td>";
				});
				_return += "</tr>";
			});

			return _return;

		}

	});

})( jQuery );
